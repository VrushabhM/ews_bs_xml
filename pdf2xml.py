#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 02:00:02 2018

@author: vrushabh

"""

import os
import subprocess
import xmlextract



#inputfile = 'BS_Coffee-day-annual-report-2015_page_44.pdf'
def to_xml(inputfile):
    outputfile = inputfile.split('.pdf')[0]+'.xml'
    cmd = 'pdftohtml -c -hidden -xml '+inputfile+' '+outputfile
    os.popen(cmd)
    
    return outputfile


xml_file = to_xml(inputfile)
obj = xmlextract.PDFtoXML()
xlsx_file = obj.main(xml_file)

# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 21:27:42 2017

@author: vrushabh
"""

import pandas
import codecs
import re
from dateutil import parser
from datetime import datetime
import numpy as np
import sqlalchemy
engine = sqlalchemy.create_engine('mssql+pyodbc://sa:miswak@365@DBServer2016_BS')
engine2 = sqlalchemy.create_engine('mssql+pyodbc://sa:miswak365@VrushabhPC')


def delete_col(df,value):
    for col in df.columns:
        temp_df = df[col];temp_df.dropna(inplace=True);temp_df = temp_df[:7]
        temp_df = temp_df.apply(lambda x:x.lower() if isinstance(x,str) else x)
        temp = temp_df.str.contains(value)
        temp = temp.dropna()
        temp = [i for i in temp if i]
        if temp:
            df.drop(col,axis=1,inplace=True)
            break
    
    return df


def get_value(df):   
    #pattern = 'in (lacs|lakhs|crore|million)'
    pattern = 'in (crore|million)'
    df = df.applymap(lambda x:str(x))
    l = df.applymap(lambda x: x if re.search(pattern,x,re.IGNORECASE) else np.nan)
    
    l.dropna(how='all',inplace=True)
    l.dropna(axis=1,how='all',inplace=True)
    
    if len(l):
        amount = l.loc[l.index[0]].values[0]
        amount = ' '.join(re.findall(r'\w+',amount)).lower()

        if amount.count('million'):
            return 1000000
        elif amount.count('crore'):
            value = 10000000/100000.0
            return value
    else:
        return 1
            

def get_index(df):
    df = df.applymap(lambda x:str(x))
    last_index = 0
    df = df.applymap(lambda x:x.lower() if x else '')
    for col in df.columns:
        temp = df.loc[(df[col].str.contains('as per our')) |(df[col].str.lower()=='total')|(df[col].str.contains('significant accounting')) ]
        if temp.size:
            last_index = temp.index[-1]            

    return last_index


def format_dataframe(df,thresh = 3):
    temp_df = df.applymap(lambda x:str(x))
    temp_df = temp_df.applymap(lambda x:' '.join(re.findall(r'\w+',x)).strip() if not re.search(r'\d+',x) else x)
    temp_df = delete_col(temp_df,'note')
    
    temp_df.columns = [str(i) for i in range(len(temp_df.columns)) ]
    cols = list(temp_df.columns)
    
    temp = []
    for ind in temp_df.index:
        d = [[ind,col] for col in cols if len(temp_df.at[ind,col]) > thresh]
        if len(d):
            temp.append(d[0])
    
    temp_df = df.applymap(lambda x:str(x))
    temp_df = delete_col(temp_df,'note')
    temp_df.columns = [str(i) for i in range(len(temp_df.columns)) ]
    cols = list(temp_df.columns)
    
    main_df = pandas.DataFrame()
    for item in temp:    
        temp_cols = cols[cols.index(item[1]):]
        d = temp_df.loc[item[0],temp_cols];d = d.apply(lambda x: x if x else np.nan)
        d.dropna(inplace=True);d.index = range(len(d.index))
        main_df=main_df.append(d,ignore_index=True)
        
    return main_df
    

def get_date(df):
    df = df.applymap(lambda x:str(x))

    #date_pattern = r'(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sep(tember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)(|\s+\d{1,2})(\s+\d{4})'
    date_pattern = r'(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)|(|\s+\d{1,2})(\s+\d{4})'    
    date_pattern2 = r'(\d{1,2}\s+)(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)(\s+\d{4})'
    pattern = '\d{1,2} \d{1,2} \d{1,4}'
    dates = []
    for ind in range(6):
        for i in df.columns:
            s = ' '.join(re.findall('\w+',df.at[ind,i]))
            if re.search(pattern,s,re.IGNORECASE):
                l = re.findall(pattern,s,re.IGNORECASE)
                date = ' '.join(l)
                #date = ' '.join([' '.join(filter(None,i)) for i in l])
                dates.append(date)

            elif re.search(date_pattern2,s,re.IGNORECASE):
                l = re.findall(date_pattern2,s,re.IGNORECASE)
                date = ' '.join([' '.join(filter(None,i)) for i in l])
                dates.append(date)

            elif re.search(date_pattern,s,re.IGNORECASE):
                l = re.findall(date_pattern,s,re.IGNORECASE)
                date = ' '.join([' '.join(filter(None,i)) for i in l])
                dates.append(date)

    return dates


def save_in_database(df,company_name,month_years,mul,writter,sheetName):

    cols = list(df.columns)
    d = df[cols[0]].apply(lambda x: ' '.join(re.findall(r'\w+',str(x),re.IGNORECASE)).title())
    
    result = pandas.DataFrame()
    for col in cols[1:]:
        df1 = pandas.concat([d,df[col]],axis=1)
        df1.columns = range(len(df1.columns))
        df1['Year'] = month_years.pop(0)
        result = result.append(df1,ignore_index=1)
    
#    result = result[pandas.notnull(result[1])]
#    result = result[result[0] != 'Nan']
    result = result.rename(columns={0:'Label',1:'Value'})
    result['Value'] = result['Value'].apply(lambda x: x*mul if isinstance(x,float) else x)
    result['Quarter']     = '12mths'
    result['ValuesIN']    = 'Rs Lacs'
    result['Name']        = company_name
    result['DateFetched'] = datetime.now()
    result.to_excel(writter,sheet_name=sheetName)
#    try:
#          result.to_sql(name='PDFBalanceSheet3',con=engine2,if_exists = 'append',index = False)#,dtype = TypeDict)    
#    except:
#          pass
    print("Saved in database")
    return result


def to_float(x):
    s = x.replace(',','').replace('(','').replace(')','')
    try:
        return(float(s))
    except:
        return x


def get_index_float(final):
    flag = 0
    for ind in final.index:
        for col in final.columns:
            if isinstance(final.at[ind,col],float):
                d = ind;flag = 1;break
        if flag:
            break
    return d


def main(df,company_name,writter,sheetName):
    cols = []
    for i in range(len(df.columns)):
        cols.append(str(i))

    df.columns = cols
    df.fillna('',inplace=True)
    dates = get_date(df)
    date_times = [parser.parse(i,dayfirst=True) for i in dates] 
    month_years = [date.strftime('%b')+'.'+date.strftime('%y') for date in date_times]
    month_years = sorted(set(month_years), key=lambda x: month_years.index(x))
    
    last_index = get_index(df)
    mul = get_value(df)
    #df = delete_col(df,'note')
    temp_df = df[:last_index]
    main_df = format_dataframe(temp_df,thresh=4)
    final = main_df.dropna(how='all',thresh=len(main_df)-30,axis=1)
    #final = final.dropna(how='any',thresh=len(main_df)-3)
    final.fillna('',inplace=True)
    final  = final.applymap(lambda x:to_float(x))
    ind = get_index_float(final)
    final = final[ind:]
    final = final.applymap(lambda x: np.nan if not x else x)
    result = save_in_database(final,company_name,month_years,mul,writter,sheetName)
    return result,date_times
    


#file_name = 'BS_Mangalore Chemicals 2017_page_48.xlsx'
#df2 = pandas.read_excel(file_name)
#df = pandas.read_excel('BS_Audited_Shilpi Cables_2015_page_92.xlsx')
#df = pandas.read_excel('BS_Coffee-day-annual-report-2015_page_44.xlsx')
#df = pandas.read_excel('BS_NCC Annual Report2014_15_page_62.xlsx')
#df = pandas.read_excel('BS_SIL_Investments_page_55.xlsx')
#df = pandas.read_excel('sample.xlsx')
#df = pandas.read_excel('BS_NCC Annual Report2014_15_page_62.xlsx')
#
#
#key = """non current liabilities,(1) Non-current assets,Financial Assets,
#       (2) Current assets,Financial Assets"""
#
#col = 0
#temp_df = final[0].apply(lambda x: unicode(x).lower())
#temp_df = temp_df.apply(lambda x:' '.join(re.findall('\w+',x)))
#temp = temp_df.loc[temp_df.str.contains(key)]
#
##d = df.loc[item[0],temp_cols];
##[[ind,col] for col in cols if len(df.at[ind,col]) > 3][0]
##[[ind,col] for col in cols if  isinstance(df.at[ind,col],unicode) &len(df.at[ind,col]) > 3]
#
#
#"profit and loss"
#df = pandas.read_excel('PnL_Audited_Shilpi Cables_2015_page_93.xlsx')
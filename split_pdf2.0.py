# -*- coding: utf-8 -*-
"""
Created on Wed Oct 25 10:38:23 2017

@author: DM390
"""
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice, TagExtractor
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import XMLConverter, HTMLConverter, TextConverter
from pdfminer.cmapdb import CMapDB
from pdfminer.layout import LAParams
from pdfminer.image import ImageWriter

#from cStringIO import StringIO
#from StringIO import StringIO
from datetime import datetime
from io import BytesIO
import re,os,getopt,sys
from PyPDF2 import PdfFileWriter, PdfFileReader
import pandas
#from pdfsplit import splitPages
from pandas import read_excel
#from gevent.threadpool import ThreadPool
#import gevent,subprocess
#import pdf_tables
#import read_html
#import mapping
import time
import mark1,mark3
import xml_BS_PnL3
import sqlalchemy

engine = sqlalchemy.create_engine('mssql+pyodbc://sa:miswak@365@DBServer2016_BS')
engine2 = sqlalchemy.create_engine('mssql+pyodbc://sa:miswak365@VrushabhPC')

""" split pdf according to content """

def get_text(infile, page,LA_analysis=False):
    output = BytesIO()
    #output = StringIO()
    manager = PDFResourceManager()
    if LA_analysis:
        converter = TextConverter(manager, output)#, laparams=LAParams())
    else:
        converter = TextConverter(manager, output)
        
    interpreter = PDFPageInterpreter(manager, converter)
    
    for page in PDFPage.get_pages(infile, set([page])):
        interpreter.process_page(page)
    converter.close()
    text = output.getvalue()
    output.close()
    return text 

#def split_pdf_pages(path,inputfile,page_range):
#      out_file = inputfile.split('.pdf')[0]+'_pageRange_'+'to'.join([str(i) for i in page_range])+'.pdf'
#      inputstream  = PdfReader(inputfile)
#      outputstream = PdfWriter()      
#      outputstream.addpages([inputstream.pages[i] for i in range(page_range[0],page_range[1])])
#      
#      outputstream.write(out_file)
#      return out_file


def save_pages2(path,inputfile,found_pages):
      inputpdf = PdfFileReader(open(inputfile, "rb"))

      #output = PdfFileWriter()
      temp = []
      for f_name,pages in found_pages.items():
            for page in pages:
                  output = PdfFileWriter()
                  out_file = path+'\\'+f_name.split('.pdf')[0]+'_page_'+str(page+1)+'.pdf'
                  output.addPage(inputpdf.getPage(page))
                  with open(out_file, "wb") as outputStream:
                        output.write(outputStream)
                  temp.append(out_file)

      return temp


def save_pages(path,inputfile,found_pages):
    temp = []
    for f_name,pages in found_pages.items():
        for page in pages:
            out_file = path+'\\'+f_name.split('.pdf')[0]+'_page_'+str(page+1)+'.pdf'
            splitPages(path=inputfile,
                       slices=[slice(page,page+1,None)],
                       outputPat=out_file)
            temp.append(out_file)          

    return temp
    
    
def count_match_kw(keywords,text):
    """count number of keyword mathched in text"""
    
    pattern = '('+'|'.join(keywords)+')'
    match = re.findall(pattern,text,re.IGNORECASE)
    l = list(set([i.lower() for i in match]))
    
    match_count = 0
    for i in keywords:
        if l.count(i):
            match_count += 1

    return match_count


def is_balance_sheet(text):
    
    BS_keywords = ['balance sheet', "shareholders  fund", 'non current liabilities',
                 'current liabilities', 'assets', 'non current assets', 'current assets']

    t = re.findall(r'\w+',text)
    text = ' '.join(t)                 
    match_count = count_match_kw(BS_keywords,text)
    if match_count == len(BS_keywords)-1:
        return True


def is_PnL(text):
    PnL_keywords = ['profit and loss','revenue','other income','total revenue',
                    'expenses','finance costs','before tax','tax expense']

    match_count = count_match_kw(PnL_keywords,text)                 
    if match_count >= len(PnL_keywords)-1:
        return True
    

def split_pages(file_name):
    pages_found = {}
    print("Started for "+file_name)
    infile = open(file_name, 'rb')
    file_name = file_name.split('\\')[-1]
    # get total number of pages
    total_pages = PdfFileReader(infile).numPages
    print("Total pages "+str(total_pages))

    for page in range(total_pages):
        #print('page no: '+str(page)+',\t')
        text = get_text(infile,page)
        #text = text.replace('&','and')
        text = text.decode('utf-8',errors='ignore')
        text = re.sub(r'[^\w]',' ',text)
    
        if is_balance_sheet(text):
            print("BS found in "+ str(page))
            if "BS_"+file_name in pages_found:
                pages_found["BS_"+file_name].append(page)
            else:
                pages_found["BS_"+file_name] = [page]
    
        if is_PnL(text):
            print("PnL found in "+ str(page))
            if "PnL_"+file_name in pages_found:
                pages_found["PnL_"+file_name].append(page)
            else:
                pages_found["PnL_"+file_name] = [page]
        
        if page%10 == 0:
              print(str(page)+' pages done,\t')

    infile.close()
    return pages_found


def make_dir(in_file,out_file):
    if not os.path.isdir(in_file.split('.pdf')[0]):
        os.mkdir(in_file.split('.pdf')[0])
        os.mkdir(in_file.split('.pdf')[0]+'\\splited_pages')
        return in_file.split('.pdf')[0]+'\\splited_pages'
    else:
        print('Directory already exists')
        sys.exit()


def clean_pnl(df):
    temp_df = df.applymap(str)
    last_index = df.index[-1]
    temp_df = temp_df.applymap(lambda x:x.lower())
    for col in temp_df.columns:
        temp = temp_df.loc[temp_df[col].str.contains('as per our')]
        if temp.size:
            last_index = temp.index[-1]

    return last_index-1


def main(argv):
    #obj = xmlextract.PDFtoXML()
    inputfile = ''
    outputfile = ''
    company_name = ''
    verbose = False
    try:
       opts, args = getopt.getopt(argv,"i:o:n:v",["ifile=","ofile=","company_name="])
    except getopt.GetoptError:
       print('argMent.py -i <inputfile> -o <outputfile>')
       sys.exit(2)
      
    for opt, arg in opts:
       print("opt,args: ",opt, arg)
       if opt == '-h':
           print('argMent.py -i <inputfile> -o <outputfile>')
           sys.exit()
       elif opt in ("-i", "--ifile"):
           inputfile = arg

       elif opt in ("-o", "--ofile"):
           outputfile = arg

       elif opt in ("-n", "--company_name"):
           company_name = arg

       elif opt == "--verbose":
           if arg in ('true','True'):
               verbose = True
           else:
               verbose = False

#    key_words = list(pandas.read_excel('balance_sheet_keywords.xlsx')['Label'])
#    labels = [x.lower() for x in key_words]
    print(inputfile,outputfile)
    path = make_dir(inputfile,outputfile)
    os.chdir(path)
    file_name = inputfile.split('\\')[-1]

    pages_found = split_pages(inputfile)
    
    splited_files = save_pages2(path,inputfile,pages_found)

    print( ', '.join(splited_files))
    converted_files = []
    for splited_file in splited_files:
        print('Extracting....')

        temp_path = '\\'.join(splited_file.split('\\')[:-1])+'\detect2'
        if not os.path.isdir(temp_path):
            os.mkdir(temp_path)

        excel_path = temp_path+'\\'+splited_file.split('\\')[-1]
        excel_path = excel_path.split('.pdf')[0]+'.xlsx'

        #xfile_path = pdf_tables.to_excel(splited_file)
        xml_file = xml_BS_PnL3.to_xml(splited_file);time.sleep(0.3)
        xfile_path,_ = xml_BS_PnL3.get_xml_data(xml_file)
        
        print(xfile_path)
        converted_files.append(xfile_path)
        print("saved "+str(xfile_path))


    result_file = inputfile.split('.pdf')[0]+'\\'+company_name+'.xlsx'
    writter = pandas.ExcelWriter(result_file)
    cnt = 1
    otherDetMain = pandas.DataFrame()
    for xfile_path in converted_files:
        df_pdfTab = read_excel(xfile_path)
        # read CA
        last_index = df_pdfTab.index[-1]
        if xfile_path.split('\\')[-1].startswith('BS'):
            sheetName = 'BalanceSheet'+str(cnt)
            
            temp_df = mark1.split_cells(df_pdfTab)
            last_index = mark3.get_index(temp_df)
            other_df = mark1.get_details(temp_df,excel_path,last_index)
            otherDetMain = otherDetMain.append(other_df,ignore_index=True)
            #writter = pandas.ExcelWriter(result_file)
            result,date_times=mark3.main(temp_df,company_name,writter,sheetName)            
            other_df.to_excel(writter,sheet_name=sheetName+'_OtherDetails')

        elif xfile_path.split('\\')[-1].startswith('PnL'):
            sheetName = 'ProfitAndLoss'+str(cnt)
            temp_df = mark1.split_cells(df_pdfTab)
            last_index = mark3.get_index(temp_df)
            other_df = mark1.get_details(temp_df,excel_path,last_index)
            otherDetMain = otherDetMain.append(other_df,ignore_index=True)
            #writter = pandas.ExcelWriter(result_file)
            result,date_times = mark3.main(temp_df,company_name,writter,sheetName)
            other_df.to_excel(writter,sheet_name=sheetName+'_OtherDetails')
        cnt +=1
        df_pdfTab = df_pdfTab[:last_index+1]
        df_pdfTab.fillna('',inplace=True)
        df_pdfTab.to_excel(excel_path)
        print("Done")

#    years = [str(date.year) for date in date_times]
#    ca_df = pandas.DataFrame(data=[[' ',' ',' ',' ']],columns= ['FirmName','FirmNo','Name','MembershipNo'])
#    #otherDetMain.drop_duplicates(inplace=True)
#
#    caDF = pandas.DataFrame()
#    for ind in other_df.index:
#          d =  other_df.iloc[ind]
#          if re.search('Chartered Accountant',d[0]):
#                ca_df['FirmName'] = d[1]
#                ca_df['FirmNo'] = d[2].replace('FRN:','').replace('Firm Reg. No.','').replace('Firm Regn. No.','').replace("Firm’s Registration Number:",'').replace('ICAI Firm registration number:','')
#          elif re.search('Partner',d[0]):
#                ca_df['Name'] = d[1]
#                ca_df['MembershipNo'] = d[2].replace('M.No.','').replace('Membership No.','').replace("Membership Number",'')
#
#    for year in years:
#          ca_df['Year'] = year
#          caDF = caDF.append(ca_df)
#
#    caDF['CompanyName'] = company_name
#    caDF['DateFetched'] = datetime.now()
#    caDF.to_sql(name='PDFAuditor',con=engine2,if_exists = 'append',index = False)#,dtype = TypeDict)    

main(sys.argv[1:])
    
#splited_file = 'F:\split_pdf\NCC Annual Report2014_15\splited_pages\BS_NCC Annual Report2014_15_page_62.pdf'

#xfile_path = 'F:\split_pdf\Audited_Shilpi Cables_2015\splited_pages\BS_Audited_Shilpi Cables_2015_page_92.xlsx'
#folder = 'splited_pages'
#file_name = 'Coffee-day-annual-report-2015.pdf'
#
#if not os.path.isdir(file_name.split('.pdf')[0]):
#    os.mkdir(file_name.split('.pdf')[0])
#    os.mkdir(file_name.split('.pdf')[0]+'/'+folder)
#    
#
#pages_found = split_pages(file_name)
#splited_files = save_pages(folder,file_name,pages_found)
#
#in_file = ('F:/split_pdf/'+splited_files[3]).replace("/","\\")
#out_file  = ('F:/split_pdf/'+splited_files[3].split('.pdf')[0]+'.htm').replace('/','\\')
#cmd = 'Release\SampleConvert.exe '+'"'+in_file+'"'+' '+'"'+out_file+'"'
#subprocess.call(cmd)
#
#df_word = read_html.read_table(out_file)
#
#file_path = in_file.split('.pdf')[0]+'.xlsx'
#file_path = pdf_tables.to_excel(in_file)
#
#df_pdfTab = read_excel(file_path)
#df_pdfTab.fillna('',inplace=True)

#cmd = 'Release\SampleConvert.exe "splited_pages\BS_NCC Annual Report2014_15_page_62.pdf" "splited_pages\BS_NCC Annual Report2014_15_page_62.htm"'
#'BS_NCC Annual Report2014_15_page_62_2.htm'
#file_list = ['ABRL Financials FY 14-15.pdf','Cabcon_xchange.pdf','Coffee-day-annual-report-2015.pdf'
#            ,'NCC Annual Report2014_15.pdf','SIL_Investments.pdf','Delton Cables_Annual_Report_2015.pdf']


#    pool = ThreadPool(4)
#    pages = get_slice(total_pages)
#    for page in pages:
#        pool.map(get_pages,page)
#        gevent.wait()
#

